package com.example.demo.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author ：lichuanbin
 * @since : 2021/4/1 10:26
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Goods {
    /**
     * 商品id
     */
    @TableId(type = IdType.AUTO)
    private Long goodsId;
    /**
     * 商品标题
     */
    private String goodsName;
    /**
     * 商品价格
     */
    private Integer goodsPrice;
    /**
     * 首页显示
     */
    private Integer isDisplay;
    /**
     * 商品描述
     */
    private String goodsDescription;
    /**
     * 商品状态
     */
    private Integer goodsStatus;
    /**
     * 游戏id
     */
    private Long gameId;
    /**
     * 游戏类型
     */
    private String typeName;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Integer ctime;
    /**
     * 修改时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Integer utime;
    /**
     * 逻辑删除 0-未删除 1-删除
     */
    @TableLogic
    private Integer isDelete;

}
