package com.example.demo.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author ：lichuanbin
 * @since : 2021/4/1 10:27
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Notice {
    /**
     * 公告id
     */
    @TableId(type = IdType.AUTO)
    private Long noticeId;
    /**
     * 公告标题
     */
    private String noticeTitle;
    /**
     * 公告副标题
     */
    private String noticeSubTitle;
    /**
     * 公告图片url
     */
    private String noticeImageUrl;
    /**
     * 公告正文
     */
    private String noticeContent;
    /**
     * 公告状态 0-未发布 1-已发布
     */
    private Integer noticeStatus;
    /**
     * 发布时间
     */
    private Integer ptime;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Integer ctime;
    /**
     * 修改时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Integer utime;
    /**
     * 逻辑删除 0-未删除 1-删除
     */
    @TableLogic
    private Integer isDelete;
}
