package com.example.demo.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author ：lichuanbin
 * @since : 2021/4/1 10:27
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Image {
    /**
     * 图片id
     */
    @TableId(type = IdType.INPUT)
    private String imageId;
    /**
     * 图片路径
     */
    private Object imageUrl;
    /**
     * 商品id
     */
    private Long goodsId;
    /**
     * 轮播id
     */
    private Long bannerId;
    /**
     * 公告id
     */
    private Long noticeId;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Integer ctime;
    /**
     * 修改时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Integer utime;
    /**
     * 逻辑删除 0-未删除 1-删除
     */
    @TableLogic
    private Integer isDelete;
}
