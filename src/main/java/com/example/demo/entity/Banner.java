package com.example.demo.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author ：lichuanbin
 * @since : 2021/4/1 10:27
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Banner {
    /**
     * 轮播id
     */
    @TableId(type = IdType.AUTO)
    private Long bannerId;
    /**
     * 轮播图片url
     */
    private String bannerImageUrl;
    /**
     * 轮播图链接
     */
    private String bannerLinkUrl;
    /**
     * 排序id
     */
    private Integer bannerOrderId;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Integer ctime;
    /**
     * 修改时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Integer utime;
    /**
     * 逻辑删除 0-未删除 1-删除
     */
    @TableLogic
    private Integer isDelete;
}
