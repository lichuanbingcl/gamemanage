package com.example.demo.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author ：lichuanbin
 * @since : 2021/4/1 10:29
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Game {
    /**
     * 游戏id
     */
    @TableId(type = IdType.AUTO)
    private Long gameId;
    /**
     * 游戏名
     */
    private String gameName;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Integer ctime;
    /**
     * 修改时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Integer utime;
    /**
     * 逻辑删除 0-未删除 1-删除
     */
    @TableLogic
    private Integer isDelete;
}
