package com.example.demo.vo;

import lombok.Data;

/**
 * @author ：lichuanbin
 * @since : 2021/4/2 15:09
 */
@Data
public class BannerListVO {
     private Long bannerId;
     private String bannerImageUrl;
     private String bannerLinkUrl;
     private Integer bannerOrderId;
     private Integer ctime;
}
