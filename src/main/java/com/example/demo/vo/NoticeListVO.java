package com.example.demo.vo;

import lombok.Data;

/**
 * @author ：lichuanbin
 * @since : 2021/4/2 14:39
 */
@Data
public class NoticeListVO {
    /**
     * 公告id
     */
    private Long noticeId;
    /**
     * 公告标题
     */
    private String noticeTitle;
    /**
     * 公告副标题
     */
    private String noticeSubTitle;
    /**
     * 公告图片url
     */
    private String noticeImageUrl;
    /**
     * 公告正文
     */
    private String noticeContent;
    /**
     * 公告状态 0-未发布 1-已发布
     */
    private Integer noticeStatus;
    /**
     * 创建时间
     */
    private Integer ctime;
    /**
     * 发布时间
     */
    private Integer ptime;
}
