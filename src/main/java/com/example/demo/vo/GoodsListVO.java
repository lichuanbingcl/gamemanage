package com.example.demo.vo;

import lombok.Data;
import java.util.List;

/**
 * @author ：lichuanbin
 * @since : 2021/4/2 16:49
 */
@Data
public class GoodsListVO {
    /**
     * 商品id
     */
    private Long goodsId;
    /**
     * 商品标题
     */
    private String goodsName;
    /**
     * 商品价格
     */
    private Integer goodsPrice;
    /**
     * 首页显示
     */
    private Integer isDisplay;
    /**
     * 商品描述
     */
    private String goodsDescription;
    /**
     * 商品状态
     */
    private Integer goodsStatus;
    /**
     * 游戏名
     */
    private String gameName;
    /**
     * 游戏类型
     */
    private String typeName;
    /**
     * 创建时间
     */
    private Integer ctime;
    /**
     * 图片路径
     */
    private List<String> imageUrls;
}
