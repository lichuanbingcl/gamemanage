package com.example.demo.vo;

import lombok.Data;

/**
 * @author ：lichuanbin
 * @since : 2021/4/2 11:33
 */
@Data
public class UserListVO {
    private Long userId;
    private String userAccount;
    private String password;
}
