package com.example.demo.vo;

import lombok.Data;

/**
 * @author ：lichuanbin
 * @since : 2021/4/7 11:01
 */
@Data
public class ImageVO {
    /**
     * 返回标记id
     */
    private Integer imageId;
    /**
     * 返回图片
     */
    private Object image;
}
