package com.example.demo.vo;

import lombok.Data;

/**
 * @author ：lichuanbin
 * @since : 2021/4/1 16:51
 */
@Data
public class GameListVO {
    private Long gameId;
    private String gameName;
}
