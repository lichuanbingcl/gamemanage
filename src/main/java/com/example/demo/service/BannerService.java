package com.example.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.demo.entity.Banner;
import com.example.demo.query.WebBannerUpdateQuery;
import com.example.demo.utils.ResponseUtils;

import java.util.List;

/**
 * @author ：lichuanbin
 * @since : 2021/4/1 11:48
 */
public interface BannerService extends IService<Banner> {
    /**
     * 添加banner
     * @param bannerImageUrl
     * @param bannerLinkUrl
     * @return
     */
    ResponseUtils addBanner(String bannerImageUrl,String bannerLinkUrl);

    /**
     * 修改banner
     * @param updateQuery
     * @return
     */
    ResponseUtils updateBanner(WebBannerUpdateQuery updateQuery);

    /**
     * banner列表
     * @return
     */
    ResponseUtils bannerList(Integer currentPage,Integer pageSize);

    /**
     * 拖动排序
     * @param orderIds
     * @return
     */
    ResponseUtils dragBanner(List<Long> orderIds);
}
