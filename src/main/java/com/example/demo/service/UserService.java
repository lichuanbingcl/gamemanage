package com.example.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.demo.entity.User;
import com.example.demo.query.WebAuthQuery;
import com.example.demo.query.WebUserUpdateQuery;
import com.example.demo.utils.ResponseUtils;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author ：lichuanbin
 * @since : 2021/4/1 11:59
 */
public interface UserService extends IService<User> {
    /**
     * 添加用户
     * @param userAccount
     * @param password
     * @return
     */
    ResponseUtils addUser(String userAccount,String password);


    /**
     * 用户列表
     * @return
     */
    ResponseUtils userList(Integer currentPage,Integer pageSize);

    /**
     * 登录
     * @param request
     * @param response
     * @param authQuery
     * @return
     */
    ResponseUtils login(HttpServletRequest request, HttpServletResponse response, WebAuthQuery authQuery);

    /**
     * 登出
     * @param request
     * @param response
     * @return
     */
    ResponseUtils logout(HttpServletRequest request, HttpServletResponse response);

}
