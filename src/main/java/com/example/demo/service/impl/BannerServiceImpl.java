package com.example.demo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.demo.entity.Banner;
import com.example.demo.entity.Image;
import com.example.demo.mapper.BannerMapper;
import com.example.demo.mapper.ImageMapper;
import com.example.demo.query.WebBannerUpdateQuery;
import com.example.demo.service.BannerService;
import com.example.demo.utils.MyPage;
import com.example.demo.utils.ResponseUtils;
import com.example.demo.vo.BannerListVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ：lichuanbin
 * @since : 2021/4/1 11:49
 */
@Service
public class BannerServiceImpl extends ServiceImpl<BannerMapper, Banner> implements BannerService {

    @Autowired
    private BannerService bannerService;
    @Autowired
    private BannerMapper bannerMapper;
    @Autowired
    private ImageMapper imageMapper;
    /**
     * 添加banner
     * @param bannerImageUrl
     * @param bannerLinkUrl
     * @return
     */
    @Transactional(rollbackFor = {RuntimeException.class, Error.class})
    @Override
    public ResponseUtils addBanner(String bannerImageUrl, String bannerLinkUrl) {
        ResponseUtils responseUtils=new ResponseUtils();
        Banner banner=new Banner();
        banner.setBannerImageUrl(bannerImageUrl);
        banner.setBannerLinkUrl(bannerLinkUrl);
        int bannerFlag=bannerMapper.insert(banner);
        Image image=new Image();
        image.setImageId(bannerImageUrl);
        image.setBannerId(banner.getBannerId());
        imageMapper.updateById(image);
        return responseUtils.success(bannerFlag==1);
    }

    /**
     * 修改banner
     *
     * @param updateQuery
     * @return
     */
    @Transactional(rollbackFor = {RuntimeException.class, Error.class})
    @Override
    public ResponseUtils updateBanner(WebBannerUpdateQuery updateQuery) {
        ResponseUtils responseUtils=new ResponseUtils();
        Banner banner=new Banner();
        BeanUtils.copyProperties(updateQuery,banner);
        boolean bannerFlag=bannerService.updateById(banner);
        QueryWrapper<Image> wrapper=new QueryWrapper<>();
        wrapper.eq("banner_id",updateQuery.getBannerId());
        imageMapper.delete(wrapper);
        Image image=new Image();
        image.setImageId(updateQuery.getBannerImageUrl());
        image.setBannerId(updateQuery.getBannerId());
        imageMapper.updateById(image);
        return responseUtils.success(bannerFlag);
    }

    /**
     * banner列表
     *
     * @return
     */
    @Override
    public ResponseUtils bannerList(Integer currentPage,Integer pageSize) {
        ResponseUtils responseUtils=new ResponseUtils();
        IPage<Banner> bannerIPage=new Page<>(currentPage,pageSize);
        QueryWrapper<Banner> wrapper=new QueryWrapper<>();
        wrapper.orderByAsc("banner_order_id").orderByDesc("ctime");
        bannerIPage=bannerMapper.selectPage(bannerIPage,wrapper);
        MyPage<BannerListVO> voMyPage=new MyPage<>();
        List<BannerListVO> vos=new ArrayList<>();
        bannerIPage.getRecords().forEach(banner -> {
            BannerListVO vo=new BannerListVO();
            BeanUtils.copyProperties(banner,vo);
            vos.add(vo);
        });
        voMyPage.setTotal(bannerIPage.getTotal());
        voMyPage.setList(vos);
        return responseUtils.success(voMyPage);
    }

    /**
     * 拖动排序
     *
     * @param orderIds
     * @return
     */
    @Override
    public ResponseUtils dragBanner(List<Long> orderIds) {
        ResponseUtils responseUtils=new ResponseUtils();
        List<Banner> banners=new ArrayList<>();
        for (int i=0;i<orderIds.size();i++){
            Banner banner=new Banner();
            banner.setBannerId(orderIds.get(i));
            banner.setBannerOrderId(i+1);
            banners.add(banner);
        }
        return responseUtils.success(bannerService.updateBatchById(banners));
    }
}
