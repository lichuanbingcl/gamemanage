package com.example.demo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.demo.entity.Image;
import com.example.demo.mapper.ImageMapper;
import com.example.demo.service.ImageService;
import com.example.demo.utils.ResponseUtils;
import lombok.experimental.Accessors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.UUID;

/**
 * @author ：lichuanbin
 * @since : 2021/4/1 11:56
 */
@Service
public class ImageServiceImpl extends ServiceImpl<ImageMapper, Image> implements ImageService {

    @Autowired
    private ImageMapper imageMapper;
    /**
     * 添加图片
     *
     * @param file
     * @return
     */
    @Override
    public ResponseUtils addImage(MultipartFile file) throws IOException {
        ResponseUtils responseUtils=new ResponseUtils();
        if (!file.isEmpty()){
            BASE64Encoder encoder=new BASE64Encoder();
            String imag=encoder.encode(file.getBytes());
            Image image=new Image();
            image.setImageUrl(imag);
            String fileName=UUID.randomUUID().toString();
            image.setImageId(fileName);
            imageMapper.insert(image);
            return responseUtils.success(fileName);
        }
        return responseUtils.success("图片上传失败");
    }

    /**
     * 获取图片
     * @param imageId
     * @param response
     */
    @Override
    public void getImage(String imageId, HttpServletResponse response) {
        try {
            Image image=imageMapper.selectById(imageId);
            byte[] imag= (byte[]) image.getImageUrl();
            String value=new String(imag, "utf-8");
            BASE64Decoder decoder=new BASE64Decoder();
            byte[] bytes = decoder.decodeBuffer(value);
            for(int i=0;i<bytes.length;i++){
                if(bytes[i]<0){
                    bytes[i]+=256;
                }
            }
            response.setContentType("image/jpg");
            ServletOutputStream out = response.getOutputStream();
            out.write(bytes);
            out.flush();
            out.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
