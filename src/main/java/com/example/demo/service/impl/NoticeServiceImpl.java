package com.example.demo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.demo.entity.Image;
import com.example.demo.entity.Notice;
import com.example.demo.mapper.ImageMapper;
import com.example.demo.mapper.NoticeMapper;
import com.example.demo.query.WebNoticeAddQuery;
import com.example.demo.query.WebNoticeListQuery;
import com.example.demo.query.WebNoticeUpdateQuery;
import com.example.demo.service.NoticeService;
import com.example.demo.utils.DateUtils;
import com.example.demo.utils.MyPage;
import com.example.demo.utils.ResponseUtils;
import com.example.demo.vo.NoticeListVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ：lichuanbin
 * @since : 2021/4/1 11:58
 */
@Service
public class NoticeServiceImpl extends ServiceImpl<NoticeMapper, Notice> implements NoticeService {

    @Autowired
    private NoticeMapper noticeMapper;
    @Autowired
    private ImageMapper imageMapper;
    /**
     * 添加公告
     * @param addQuery
     * @return
     */
    @Override
    public ResponseUtils addNotice(WebNoticeAddQuery addQuery) {
        ResponseUtils responseUtils=new ResponseUtils();
        Notice notice=new Notice();
        BeanUtils.copyProperties(addQuery,notice);
        int flag=noticeMapper.insert(notice);
        if (!addQuery.getNoticeImageUrl().isEmpty()){
            Image image=new Image();
            image.setImageId(addQuery.getNoticeImageUrl());
            image.setNoticeId(notice.getNoticeId());
            imageMapper.updateById(image);
        }
        return responseUtils.success(flag==1);
    }

    /**
     * 修改公告
     *
     * @param updateQuery
     * @return
     */
    @Override
    public ResponseUtils updateNotice(WebNoticeUpdateQuery updateQuery) {
        ResponseUtils responseUtils=new ResponseUtils();
        Notice notice=new Notice();
        BeanUtils.copyProperties(updateQuery,notice);
        QueryWrapper<Image> wrapper=new QueryWrapper<>();
        wrapper.eq("notice_id",updateQuery.getNoticeId());
        imageMapper.delete(wrapper);
        if (!updateQuery.getNoticeImageUrl().isEmpty()){
            Image image=new Image();
            image.setImageId(updateQuery.getNoticeImageUrl());
            image.setNoticeId(notice.getNoticeId());
            imageMapper.updateById(image);
        }
        return responseUtils.success(noticeMapper.updateById(notice)==1);
    }

    /**
     * 公告列表
     *
     * @param listQuery
     * @return
     */
    @Override
    public ResponseUtils noticeList(WebNoticeListQuery listQuery) {
        ResponseUtils responseUtils=new ResponseUtils();
        IPage<Notice> noticeIPage=new Page<>(listQuery.getCurrentPage(),listQuery.getPageSize());
        QueryWrapper<Notice> wrapper=new QueryWrapper<>();
        if (listQuery.getState()!=null){
            wrapper.eq("notice_status",listQuery.getState()).orderByDesc("ctime");
        }
        noticeIPage=noticeMapper.selectPage(noticeIPage,wrapper);
        List<NoticeListVO> vos=new ArrayList<>();
        noticeIPage.getRecords().forEach(notice->{
            NoticeListVO vo=new NoticeListVO();
            BeanUtils.copyProperties(notice,vo);
            vos.add(vo);
        });
        MyPage<NoticeListVO> voMyPage=new MyPage<>();
        voMyPage.setList(vos);
        voMyPage.setTotal(noticeIPage.getTotal());
        return responseUtils.success(voMyPage);
    }

    /**
     * 发布公告
     *
     * @param noticeId
     * @return
     */
    @Override
    public ResponseUtils publishNotice(Long noticeId) {
        ResponseUtils responseUtils=new ResponseUtils();
        Notice notice=noticeMapper.selectById(noticeId);
        switch (notice.getNoticeStatus()){
            case 0:
                notice.setNoticeStatus(1);
                notice.setPtime(DateUtils.currentSecond());
                break;
            case 1:
                notice.setNoticeStatus(0);
                break;
            default:
                return responseUtils.success(false);
        }
        return responseUtils.success(noticeMapper.updateById(notice)==1);
    }
}
