package com.example.demo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.demo.entity.Game;
import com.example.demo.entity.Goods;
import com.example.demo.entity.Image;
import com.example.demo.mapper.GameMapper;
import com.example.demo.mapper.GoodsMapper;
import com.example.demo.mapper.ImageMapper;
import com.example.demo.query.WebGoodsAddQuery;
import com.example.demo.query.WebGoodsSelectQuery;
import com.example.demo.query.WebGoodsUpdateQuery;
import com.example.demo.service.GoodsService;
import com.example.demo.service.ImageService;
import com.example.demo.utils.MyPage;
import com.example.demo.utils.ResponseUtils;
import com.example.demo.vo.GoodsListVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ：lichuanbin
 * @since : 2021/4/1 11:53
 */
@Service
public class GoodsServiceImpl extends ServiceImpl<GoodsMapper,Goods> implements GoodsService {

    @Autowired
    private ImageService imageService;
    @Autowired
    private GoodsMapper goodsMapper;
    @Autowired
    private ImageMapper imageMapper;
    @Autowired
    private GameMapper gameMapper;
    @Autowired
    private GoodsService goodsService;
    /**
     * 添加商品
     *
     * @param addQuery
     * @return
     */
    @Transactional(rollbackFor = {RuntimeException.class, Error.class})
    @Override
    public ResponseUtils addGoods(WebGoodsAddQuery addQuery) {
        ResponseUtils responseUtils=new ResponseUtils();
        Goods goods=new Goods();
        BeanUtils.copyProperties(addQuery,goods);
        goods.setGoodsStatus(0);
        List<Image> images=new ArrayList<>();
        addQuery.getImages().forEach(url -> {
            Image image=new Image();
            image.setGoodsId(goods.getGoodsId());
            image.setImageId(url);
            images.add(image);
        });
        return responseUtils.success(goodsMapper.insert(goods)==1&&imageService.updateBatchById(images));
    }

    /**
     * 删除商品
     *
     * @param goodsId
     * @return
     */
    @Transactional(rollbackFor = {RuntimeException.class, Error.class})
    @Override
    public ResponseUtils deleteGoods(Long goodsId) {
        ResponseUtils responseUtils=new ResponseUtils();
        QueryWrapper<Image> wrapper=new QueryWrapper<>();
        wrapper.eq("goods_id",goodsId);
        imageMapper.delete(wrapper);
        return responseUtils.success(gameMapper.deleteById(goodsId)==1);
    }

    /**
     * 修改商品
     *
     * @param updateQuery
     * @return
     */
    @Transactional(rollbackFor = {RuntimeException.class, Error.class})
    @Override
    public ResponseUtils updateGoods(WebGoodsUpdateQuery updateQuery) {
        ResponseUtils responseUtils=new ResponseUtils();
        Goods goods=new Goods();
        BeanUtils.copyProperties(updateQuery,goods);
        List<Image> images=new ArrayList<>();
        if (!updateQuery.getImages().isEmpty()){
            updateQuery.getImages().forEach(url -> {
                Image image=new Image();
                image.setGoodsId(goods.getGoodsId());
                image.setImageId(url);
                images.add(image);
            });
        }
        Map map=new HashMap();
        map.put("goods_id",updateQuery.getGoodsId());
        imageMapper.deleteByMap(map);
        return responseUtils.success(imageService.updateBatchById(images)&&goodsMapper.updateById(goods)==1);
    }

    /**
     * 获取商品列表
     *
     * @param selectQuery
     * @return
     */
    @Transactional(rollbackFor = {RuntimeException.class, Error.class})
    @Override
    public ResponseUtils goodsList(WebGoodsSelectQuery selectQuery) {
        ResponseUtils responseUtils=new ResponseUtils();
        IPage<Goods> goodsIPage=new Page<>(selectQuery.getCurrentPage(),selectQuery.getPageSize());
        QueryWrapper<Goods> goodsQueryWrapper=new QueryWrapper<>();
        QueryWrapper<Image> imageQueryWrapper=new QueryWrapper<>();
        MyPage<GoodsListVO> voMyPage=new MyPage<>();
        List<GoodsListVO> vos=new ArrayList<>();
        if (selectQuery.getGoodsId()!=null){
            Goods goods=goodsMapper.selectById(selectQuery.getGoodsId());
            GoodsListVO vo=new GoodsListVO();
            imageQueryWrapper.eq("goods_id",selectQuery.getGoodsId());
            List<Image> images=imageMapper.selectList(imageQueryWrapper);
            List<String> strings=new ArrayList<>();
            images.forEach(image -> {
                strings.add(image.getImageId());
            });
            BeanUtils.copyProperties(goods,vo);
            vo.setImageUrls(strings);
            if (goods.getGameId()!=null){
                Game game=gameMapper.selectById(goods.getGameId());
                vo.setGameName(game.getGameName());
            }
            vos.add(vo);
            voMyPage.setTotal(Long.parseLong("1"));
            voMyPage.setList(vos);
            return responseUtils.success(voMyPage);
        }

        if (!selectQuery.getKeyWord().isEmpty()){
            goodsQueryWrapper.like("goods_name",selectQuery.getKeyWord());
        }

        if (selectQuery.getGameId()!=null){
            goodsQueryWrapper.eq("game_id",selectQuery.getGameId());
        }

        if (selectQuery.getIsDisplay()!=null){
            goodsQueryWrapper.eq("is_display",selectQuery.getIsDisplay());
        }

        if (selectQuery.getGoodsStatus()!=null){
            goodsQueryWrapper.eq("goods_status",selectQuery.getGoodsStatus());
        }

        if (selectQuery.getStartPrice()!=null){
            goodsQueryWrapper.ge("goods_price",selectQuery.getStartPrice());
        }

        if (selectQuery.getEndPrice()!=null){
            goodsQueryWrapper.le("goods_price",selectQuery.getEndPrice());
        }

        if (selectQuery.getStartTime()!=null){
            goodsQueryWrapper.ge("ctime",selectQuery.getStartTime());
        }

        if (selectQuery.getEndTime()!=null){
            goodsQueryWrapper.le("ctime",selectQuery.getEndTime());
        }

        goodsQueryWrapper.orderByDesc("ctime");
        goodsIPage=goodsMapper.selectPage(goodsIPage,goodsQueryWrapper);

        goodsIPage.getRecords().forEach(goods -> {
            GoodsListVO vo=new GoodsListVO();
            BeanUtils.copyProperties(goods,vo);
            imageQueryWrapper.eq("goods_id",goods.getGameId());
            List<Image> images=imageMapper.selectList(imageQueryWrapper);
            List<String> strings=new ArrayList<>();
            images.forEach(image -> {
                strings.add(image.getImageId());
            });
            if (goods.getGameId()!=null){
                Game game=gameMapper.selectById(goods.getGameId());
                vo.setGameName(game.getGameName());
            }
            vo.setImageUrls(strings);
            vos.add(vo);
        });
        voMyPage.setList(vos);
        voMyPage.setTotal(goodsIPage.getTotal());
        return responseUtils.success(voMyPage);
    }

    /**
     * 上架、下架商品
     *
     * @param goodsId
     * @return
     */
    @Override
    public ResponseUtils riseGoods(Long goodsId) {
        ResponseUtils responseUtils=new ResponseUtils();
        Goods goods=goodsMapper.selectById(goodsId);
        switch(goods.getGoodsStatus()){
            case 0:goods.setGoodsStatus(1);break;
            case 1:goods.setGoodsStatus(0);break;
            default:return responseUtils.success(false);
        }
        return responseUtils.success(goodsMapper.updateById(goods)==1);
    }

    /**
     * 批量删除商品
     *
     * @param goodsList
     * @return
     */
    @Transactional(rollbackFor = {RuntimeException.class, Error.class})
    @Override
    public ResponseUtils batchDeleteGoods(List<Long> goodsList) {
        ResponseUtils responseUtils=new ResponseUtils();
        goodsList.forEach(goodsId->{
            QueryWrapper<Image> wrapper=new QueryWrapper<>();
            wrapper.eq("goods_id",goodsId);
            imageMapper.delete(wrapper);
        });
        return responseUtils.success(goodsMapper.deleteBatchIds(goodsList)>0);
    }

    /**
     * 批量上架、下架商品
     *
     * @param goodsList
     * @param goodsStatus
     * @return
     */
    @Override
    public ResponseUtils batchRiseGoods(List<Long> goodsList, Integer goodsStatus) {
        ResponseUtils responseUtils=new ResponseUtils();
        List<Goods> goods=new ArrayList<>();
        goodsList.forEach(id->{
            Goods goods1=new Goods();
            goods1.setGoodsId(id);
            goods1.setGoodsStatus(goodsStatus);
            goods.add(goods1);
        });
        return responseUtils.success(goodsService.updateBatchById(goods));
    }
}
