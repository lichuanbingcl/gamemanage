package com.example.demo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.demo.entity.Game;
import com.example.demo.entity.Goods;
import com.example.demo.mapper.GameMapper;
import com.example.demo.mapper.GoodsMapper;
import com.example.demo.service.GameService;
import com.example.demo.service.GoodsService;
import com.example.demo.utils.ResponseUtils;
import com.example.demo.vo.GameListVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ：lichuanbin
 * @since : 2021/4/1 11:51
 */
@Service
public class GameServiceImpl extends ServiceImpl<GameMapper, Game> implements GameService {

    @Autowired
    private GameMapper gameMapper;
    /**
     * 添加游戏
     *
     * @param gameName
     * @return
     */
    @Override
    public ResponseUtils addGame(String gameName) {
        ResponseUtils responseUtils=new ResponseUtils();
        Game game=new Game();
        game.setGameName(gameName);
        return responseUtils.success(gameMapper.insert(game)==1);
    }

    /**
     * 游戏列表
     *
     * @return
     */
    @Override
    public ResponseUtils gameList() {
        ResponseUtils responseUtils=new ResponseUtils();
        List<GameListVO> vos=new ArrayList<>();
        List<Game> games= gameMapper.selectList(null);
        games.forEach(game -> {
            GameListVO vo=new GameListVO();
            vo.setGameId(game.getGameId());
            vo.setGameName(game.getGameName());
            vos.add(vo);
        });
        return responseUtils.success(vos);
    }

    @Autowired
    private GoodsMapper goodsMapper;
    /**
     * 删除游戏
     *
     * @param gameId
     * @return
     */
    @Override
    public ResponseUtils deleteGame(Long gameId) {
        ResponseUtils responseUtils=new ResponseUtils();
        Goods goods=new Goods();
        goods.setGameId(null);
        QueryWrapper<Goods> wrapper=new QueryWrapper<>();
        wrapper.eq("game_id",gameId);
        goodsMapper.update(goods,wrapper);
        return responseUtils.success(gameMapper.deleteById(gameId)==1);
    }

}
