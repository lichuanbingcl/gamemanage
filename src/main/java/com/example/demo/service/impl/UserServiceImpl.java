package com.example.demo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.demo.entity.User;
import com.example.demo.mapper.UserMapper;
import com.example.demo.query.WebAuthQuery;
import com.example.demo.query.WebUserUpdateQuery;
import com.example.demo.service.UserService;
import com.example.demo.utils.CookieUtils;
import com.example.demo.utils.JwtUtils;
import com.example.demo.utils.MyPage;
import com.example.demo.utils.ResponseUtils;
import com.example.demo.vo.UserListVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ：lichuanbin
 * @since : 2021/4/1 12:00
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private CookieUtils cookieUtils;
    /**
     * 添加用户
     *
     * @param userAccount
     * @param password
     * @return
     */
    @Override
    public ResponseUtils addUser(String userAccount, String password) {
        ResponseUtils responseUtils=new ResponseUtils();
        User user=new User();
        user.setPassword(password);
        user.setUserAccount(userAccount);
        QueryWrapper<User> wrapper=new QueryWrapper<>();
        wrapper.eq("user_account",userAccount);
        if (!userMapper.selectList(wrapper).isEmpty()){
            return responseUtils.success("用户名已存在");
        }
        return responseUtils.success(userMapper.insert(user)==1);
    }

    /**
     * 用户列表
     *
     * @return
     */
    @Override
    public ResponseUtils userList(Integer currentPage,Integer pageSize) {
        ResponseUtils responseUtils=new ResponseUtils();
        IPage<User> userIPage=new Page<>(currentPage,pageSize);
        userIPage=userMapper.selectPage(userIPage,null);
        List<UserListVO> vos=new ArrayList<>();
        userIPage.getRecords().forEach(user -> {
            UserListVO vo=new UserListVO();
            BeanUtils.copyProperties(user,vo);
            vos.add(vo);
        });
        MyPage<UserListVO> voMyPage=new MyPage<>();
        voMyPage.setTotal(userIPage.getTotal());
        voMyPage.setList(vos);
        return responseUtils.success(voMyPage);
    }

    /**
     * 登录
     *
     * @param request
     * @param response
     * @param authQuery
     * @return
     */
    @Override
    public ResponseUtils login(HttpServletRequest request, HttpServletResponse response, WebAuthQuery authQuery) {
        //返回值处理
        ResponseUtils responseUtils=new ResponseUtils();
        //基本判断
        if (authQuery.getUserAccount()==null||authQuery.getPassword().isEmpty()){
            return responseUtils.fail().message("成功").data("用户名或密码为空!").code(HttpStatus.OK);
        }
        QueryWrapper<User> wrapper=new QueryWrapper<>();
        wrapper.eq("user_account",authQuery.getUserAccount()).eq("password",authQuery.getPassword());
        User user=userMapper.selectOne(wrapper);
        if (user!=null){
            String token= JwtUtils.getJwtToken(authQuery.getUserAccount());
            cookieUtils.setToken(request,response,token);
            return responseUtils.success(token);
        }
        return responseUtils.success("用户名或密码错误");
    }

    /**
     * 登出
     *
     * @param request
     * @param response
     * @return
     */
    @Override
    public ResponseUtils logout(HttpServletRequest request, HttpServletResponse response) {
        ResponseUtils responseUtils=new ResponseUtils();
        String token= "退出";
        cookieUtils.setToken(request,response,token);
        return responseUtils.success(true);
    }
}
