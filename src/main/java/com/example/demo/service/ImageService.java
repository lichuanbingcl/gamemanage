package com.example.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.demo.entity.Image;
import com.example.demo.utils.ResponseUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * @author ：lichuanbin
 * @since : 2021/4/1 11:56
 */
public interface ImageService extends IService<Image> {
    /**
     * 添加图片
     * @param file
     * @return
     */
    ResponseUtils addImage(MultipartFile file) throws IOException;

    /**
     * 获取图片
     * @param imageId
     * @param response
     */
    void getImage(String imageId, HttpServletResponse response);

}
