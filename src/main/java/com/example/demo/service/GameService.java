package com.example.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.demo.entity.Game;
import com.example.demo.utils.ResponseUtils;

/**
 * @author ：lichuanbin
 * @since : 2021/4/1 11:51
 */
public interface GameService extends IService<Game> {
    /**
     * 添加游戏
     * @param gameName
     * @return
     */
    ResponseUtils addGame(String gameName);

    /**
     * 游戏列表
     * @return
     */
    ResponseUtils gameList();

    /**
     * 删除游戏
     * @param gameId
     * @return
     */
    ResponseUtils deleteGame(Long gameId);
}
