package com.example.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.demo.entity.Notice;
import com.example.demo.query.WebNoticeAddQuery;
import com.example.demo.query.WebNoticeListQuery;
import com.example.demo.query.WebNoticeUpdateQuery;
import com.example.demo.utils.ResponseUtils;

/**
 * @author ：lichuanbin
 * @since : 2021/4/1 11:57
 */
public interface NoticeService extends IService<Notice> {
    /**
     * 添加公告
     * @param addQuery
     * @return
     */
    ResponseUtils addNotice(WebNoticeAddQuery addQuery);

    /**
     * 修改公告
     * @param updateQuery
     * @return
     */
    ResponseUtils updateNotice(WebNoticeUpdateQuery updateQuery);

    /**
     * 公告列表
     * @param listQuery
     * @return
     */
    ResponseUtils noticeList(WebNoticeListQuery listQuery);

    /**
     * 发布公告
     * @param noticeId
     * @return
     */
    ResponseUtils publishNotice(Long noticeId);
}
