package com.example.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.demo.entity.Goods;
import com.example.demo.query.WebGoodsAddQuery;
import com.example.demo.query.WebGoodsSelectQuery;
import com.example.demo.query.WebGoodsUpdateQuery;
import com.example.demo.utils.ResponseUtils;

import java.util.List;

/**
 * @author ：lichuanbin
 * @since : 2021/4/1 11:52
 */
public interface GoodsService extends IService<Goods> {
    /**
     * 添加商品
     * @param addQuery
     * @return
     */
    ResponseUtils addGoods(WebGoodsAddQuery addQuery);

    /**
     * 删除商品
     * @param goodsId
     * @return
     */
    ResponseUtils deleteGoods(Long goodsId);

    /**
     * 修改商品
     * @param updateQuery
     * @return
     */
    ResponseUtils updateGoods(WebGoodsUpdateQuery updateQuery);

    /**
     * 获取商品列表
     * @param selectQuery
     * @return
     */
    ResponseUtils goodsList(WebGoodsSelectQuery selectQuery);

    /**
     * 上架、下架商品
     * @param goodsId
     * @return
     */
    ResponseUtils riseGoods(Long goodsId);

    /**
     * 批量删除商品
     * @param goodsList
     * @return
     */
    ResponseUtils batchDeleteGoods(List<Long> goodsList);

    /**
     * 批量上架、下架商品
     * @param goodsList
     * @param goodsStatus
     * @return
     */
    ResponseUtils batchRiseGoods(List<Long> goodsList,Integer goodsStatus);
}
