package com.example.demo.utils;

import org.springframework.http.HttpStatus;

import java.util.HashMap;

/**
 * @author ：lichuanbin
 * @since : 2021/3/4 15:32
 *
 * 返回值处理工具
 */
public class ResponseUtils extends HashMap<String,Object> {

    public ResponseUtils code(HttpStatus status) {
        this.put("code", status.value());
        return this;
    }

    public ResponseUtils message(String message) {
        this.put("message", message);
        return this;
    }

    public ResponseUtils data(Object data) {
        this.put("data", data);
        return this;
    }

    /**
     *  200
     * @return
     */
    public ResponseUtils success(Object data) {
        this.code(HttpStatus.OK);
        this.message("成功");
        this.data(data);
        return this;
    }

    /**
     * 500
     * @return
     */
    public ResponseUtils fail() {
        this.code(HttpStatus.INTERNAL_SERVER_ERROR);
        return this;
    }

    @Override
    public ResponseUtils put(String key, Object value) {
        super.put(key, value);
        return this;
    }

}
