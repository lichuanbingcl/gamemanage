package com.example.demo.utils;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author ：lichuanbin
 * @since : 2021/3/23 15:56
 */

public class LoginInterceptor extends HandlerInterceptorAdapter {
    @Autowired
    private CookieUtils cookieUtil;

    /**
     * 思路:
     * 1. 在登录时进入登录API, 如果登录成功, 将token信息写入cookie
     * 2. 进入后台(web)的命令时, 每次都从cookie中获取值并判断, 成功返回true, 允许进入服务.
     * @param request
     * @param response
     * @param handler
     * @return
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String token = cookieUtil.getValue(request, "token");
        if (StringUtils.isEmpty(token)){
            token="token无效！";
        }
        if (JwtUtils.checkToken(request)) {
            cookieUtil.setToken(request, response, token);
            // token 解析成功,就跳转
            return super.preHandle(request, response, handler);
        }
        if (JwtUtils.checkToken(token)) {
            cookieUtil.setToken(request, response, token);
            // token 解析成功,就跳转
            return super.preHandle(request, response, handler);
        }
        return false;
    }
}
