package com.example.demo.utils;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ：lichuanbin
 * @since : 2021/3/23 16:01
 */
@Configuration
@ComponentScan("com.example.demo")
public class BaseInterceptor implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        List<String> excludePath = new ArrayList<>();
        //排除拦截，除了注册登录(此时还没token)，其他都拦截
        //登录
        excludePath.add("/web/auth/login");
        //静态资源
        excludePath.add("/static/**");
        //静态资源
        excludePath.add("/assets/**");

        //registry.addInterceptor(corsInterceptor()).addPathPatterns("/**");
        //registry.addInterceptor(loginInterceptor()).addPathPatterns("/**").excludePathPatterns(excludePath);
    }

    @Bean
    public HandlerInterceptor loginInterceptor() {
        return new LoginInterceptor();
    }

//    @Bean
//    public CorsInterceptor corsInterceptor() {
//        return new CorsInterceptor();
//    }
}
