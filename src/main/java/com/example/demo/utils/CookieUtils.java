package com.example.demo.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

/**
 * @author ：lichuanbin
 * @since : 2021/3/23 15:57
 */
@Component
public class CookieUtils {
    @Value("${manage.cookie.domain}")
    private String domain;

    @Value("${manage.cookie.maxAge}")
    private Integer maxAge;

    public static final String TOKEN = "token";

    public void setToken(HttpServletRequest request, HttpServletResponse response, String cookieValue) {
        setCookie(request, response, TOKEN, cookieValue, "/", maxAge);
    }

    public void setCookie(HttpServletRequest request, HttpServletResponse response, String cookieName,
                          String cookieValue, String path, int maxAge) {
        try {
            Cookie cookie = new Cookie(cookieName, URLEncoder.encode(cookieValue, "utf-8"));
            cookie.setMaxAge(maxAge);
            cookie.setPath(path);
            cookie.setDomain(domain);
            cookie.setHttpOnly(true);
            response.addCookie(cookie);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }


    /**
     * 获取cookie值
     *
     * @param request
     * @param name
     * @return
     */
    public String getValue(HttpServletRequest request, String name) {
        String value = null;
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (name.equals(cookie.getName())) {
                    try {
                        value = URLDecoder.decode(cookie.getValue(), "utf-8");
                    } catch (UnsupportedEncodingException e) {
                        throw new RuntimeException(e.getMessage());
                    }
                    break;
                }
            }
        }
        return value;
    }
}
