package com.example.demo.utils;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ：lichuanbin
 * @since : 2021/3/12 15:52
 */
@Data
@AllArgsConstructor
public class MyPage<T> {

    private Long total;
    private List<T> list;

    public MyPage() {
        this.total = 0L;
        list = new ArrayList<>();
    }
}
