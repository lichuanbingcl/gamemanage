package com.example.demo.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.api.R;
import com.example.demo.entity.Game;
import com.example.demo.entity.Goods;
import com.example.demo.entity.Notice;
import com.example.demo.query.*;
import com.example.demo.service.*;
import com.example.demo.utils.ResponseUtils;
import com.example.demo.vo.GameListVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ：lichuanbin
 * @since : 2021/4/1 14:51
 */
@RestController
@RequestMapping("/web")
public class WebController {

    @Autowired
    private GoodsService goodsService;

    @Autowired
    private GameService gameService;

    @Autowired
    private UserService userService;

    @Autowired
    private NoticeService noticeService;

    @Autowired
    private BannerService bannerService;

    @Autowired
    private ImageService imageService;

    /**
     * 用户登录
     */

    @PostMapping("/auth/login")
    public ResponseUtils login(HttpServletResponse response, HttpServletRequest request, @RequestBody WebAuthQuery authQuery){
        return userService.login(request,response,authQuery);
    }

    @PostMapping("/auth/logout")
    public ResponseUtils logout(HttpServletRequest request,HttpServletResponse response){
        return userService.logout(request,response);
    }


    /**
     * 图片模块
     */

    /**
     * 添加图片
     * @param file
     * @return
     * @throws IOException
     */
    @PostMapping("/image/addImage")
    public ResponseUtils addImage(@RequestParam(value = "file") MultipartFile file) throws IOException {
        return imageService.addImage(file);
    }

    /**
     * 获取图片
     * @param imageId
     * @param response
     */
    @GetMapping("/image/getImage")
    public void getImage(@RequestParam("imageId") String imageId, HttpServletResponse response){
        imageService.getImage(imageId,response);
    }

    /**
     * 删除图片
     * @param imageId
     * @return
     */
    @PostMapping("/image/deleteImage")
    public ResponseUtils deleteImage(@RequestParam("imageId")String imageId){
        ResponseUtils responseUtils=new ResponseUtils();
        return responseUtils.success(imageService.removeById(imageId));
    }

    /**
     * banner模块
     */

    /**
     * 添加banner
     * @param bannerImageUrl
     * @param bannerLinkUrl
     * @return
     */
    @PostMapping("/banner/addBanner")
    public ResponseUtils addBanner(@RequestParam("bannerImageUrl") String bannerImageUrl,@RequestParam("bannerLinkUrl") String bannerLinkUrl){
        return bannerService.addBanner(bannerImageUrl,bannerLinkUrl);
    }

    /**
     * 删除banner
     * @param bannerId
     * @return
     */
    @PostMapping("/banner/deleteBanner")
    public ResponseUtils deleteBanner(@RequestParam("bannerId")Long bannerId){
        ResponseUtils responseUtils=new ResponseUtils();
        return responseUtils.success(bannerService.removeById(bannerId));
    }

    /**
     * 修改banner
     * @param updateQuery
     * @return
     */
    @PostMapping("/banner/updateBanner")
    public ResponseUtils updateBanner(@RequestBody WebBannerUpdateQuery updateQuery){
        return bannerService.updateBanner(updateQuery);
    }

    /**
     * banner列表
     * @return
     */
    @PostMapping("/banner/bannerList")
    public ResponseUtils bannerList(@RequestParam("currentPage") Integer currentPage,@RequestParam("pageSize") Integer pageSize){
        return bannerService.bannerList(currentPage,pageSize);
    }

    /**
     * 拖动排序
     * @param orderIds
     * @return
     */
    @PostMapping("/banner/dragBanner")
    public ResponseUtils dragBanner(@RequestParam("orderIds") List<Long> orderIds){
        return bannerService.dragBanner(orderIds);
    }


    /**
     * 公告管理
     */


    /**
     * 添加公告
     * @param addQuery
     * @return
     */
    @PostMapping("/notice/addNotice")
    public ResponseUtils addNotice(@RequestBody WebNoticeAddQuery addQuery){
        return noticeService.addNotice(addQuery);
    }

    /**
     * 修改公告
     * @param updateQuery
     * @return
     */
    @PostMapping("/notice/updateNotice")
    public ResponseUtils updateNotice(@RequestBody WebNoticeUpdateQuery updateQuery){
        return noticeService.updateNotice(updateQuery);
    }

    /**
     * 删除公告
     * @param noticeId
     * @return
     */
    @PostMapping("/notice/deleteNotice")
    public ResponseUtils deleteNotice(@RequestParam("noticeId")Long noticeId){
        ResponseUtils responseUtils=new ResponseUtils();
        return responseUtils.success(noticeService.removeById(noticeId));
    }

    /**
     * 公告列表
     * @param listQuery
     * @return
     */
    @PostMapping("/notice/noticeList")
    public ResponseUtils noticeList(@RequestBody WebNoticeListQuery listQuery){
        return noticeService.noticeList(listQuery);
    }

    /**
     * 发布公告
     * @param noticeId
     * @return
     */
    @PostMapping("/notice/publishNotice")
    public ResponseUtils publishNotice(@RequestParam("noticeId")Long noticeId){
        return noticeService.publishNotice(noticeId);
    }


    /**
     * 用户模块
     */


    /**
     * 添加用户
     * @param userAccount
     * @param password
     * @return
     */
    @PostMapping("/user/addUser")
    public ResponseUtils addUser(@RequestParam("userAccount")String userAccount,@RequestParam("password") String password){
        System.out.println(userAccount+password);
        return userService.addUser(userAccount,password);
    }

    /**
     * 删除用户
     * @param userId
     * @return
     */
    @PostMapping("/user/deleteUser")
    public ResponseUtils deleteUser(@RequestParam("userId")Long userId){
        ResponseUtils responseUtils=new ResponseUtils();
        return responseUtils.success(userService.removeById(userId));
    }

    /**
     * 用户列表
     * @return
     */
    @PostMapping("/user/userList")
    public ResponseUtils userList(@RequestParam("currentPage") Integer currentPage,@RequestParam("pageSize") Integer pageSize){
        return userService.userList(currentPage,pageSize);
    }

    /**
     * 游戏模块
     */

    /**
     * 添加游戏
     * @param gameName
     * @return
     */
    @PostMapping("/game/addGame")
    public ResponseUtils addGame(@RequestParam("gameName") String gameName){
        return gameService.addGame(gameName);
    }

    /**
     * 获取游戏列表
     * @return
     */
    @PostMapping("/game/gameList")
    public ResponseUtils gameList(){
        return gameService.gameList();
    }

    /**
     * 删除游戏
     * @param gameId
     * @return
     */
    @PostMapping("/game/deleteGame")
    public ResponseUtils deleteGame(@RequestParam("gameId")Long gameId){
        return gameService.deleteGame(gameId);
    }


    /**
     * 商品模块
     */


    /**
     * 添加商品
     * @param addQuery
     * @return
     */
    @PostMapping("goods/addGoods")
    public ResponseUtils addGoods(@RequestBody WebGoodsAddQuery addQuery){
        return goodsService.addGoods(addQuery);
    }

    /**
     * 删除商品
     * @param goodsId
     * @return
     */
    @PostMapping("goods/deleteGoods")
    public ResponseUtils deleteGoods(@RequestParam("goodsId")Long goodsId){
        return goodsService.deleteGoods(goodsId);
    }

    /**
     * 修改商品
     * @param updateQuery
     * @return
     */
    @PostMapping("goods/updateGoods")
    public ResponseUtils updateGoods(@RequestBody WebGoodsUpdateQuery updateQuery){
        return goodsService.updateGoods(updateQuery);
    }

    /**
     * 获取商品列表
     * @param selectQuery
     * @return
     */
    @PostMapping("/goods/goodsList")
    public ResponseUtils goodsList(@RequestBody WebGoodsSelectQuery selectQuery){
        return goodsService.goodsList(selectQuery);
    }

    /**
     * 上架,下架商品
     * @param goodsId
     * @return
     */
    @PostMapping("/goods/riseGoods")
    public ResponseUtils riseGoods(@RequestParam("goodsId")Long goodsId){
        return goodsService.riseGoods(goodsId);
    }

    /**
     * 批量删除商品
     * @param goodsList
     * @return
     */
    @PostMapping("/goods/batchDeleteGoods")
    public ResponseUtils batchDeleteGoods(@RequestParam("goodsList")List<Long> goodsList){
        return goodsService.batchDeleteGoods(goodsList);
    }

    /**
     * 批量上架、下架商品
     * @param goodsList
     * @param goodsStatus
     * @return
     */
    @PostMapping("/goods/batchRiseGoods")
    public ResponseUtils batchRiseGoods(@RequestParam("goodsList")List<Long> goodsList,@RequestParam("goodsStatus")Integer goodsStatus){
        return goodsService.batchRiseGoods(goodsList,goodsStatus);
    }
}
