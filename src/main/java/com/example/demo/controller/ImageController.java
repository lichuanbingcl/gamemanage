package com.example.demo.controller;

import com.example.demo.entity.Image;
import com.example.demo.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;
import sun.nio.cs.UTF_32;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.UUID;

/**
 * @author ：lichuanbin
 * @since : 2021/4/6 16:58
 */
@RestController
public class ImageController {
    @Autowired
    private ImageService imageService;
    @PostMapping("/upload")
    public String upload(MultipartFile file) throws Exception{
        if (!file.isEmpty()){
            BASE64Encoder encoder=new BASE64Encoder();
            String imag=encoder.encode(file.getBytes());
            Image image=new Image();
            image.setImageUrl(imag);
            String fileName=UUID.randomUUID().toString();
            image.setImageId(fileName);
            imageService.save(image);
            return fileName;
        }
        return null;
    }

    @GetMapping("/get")
    public String get(HttpServletResponse response,String imageId) throws IOException {
        try {
            Image image=imageService.getById(imageId);
            byte[] imag= (byte[]) image.getImageUrl();
            String value=new String(imag, "utf-8");
            BASE64Decoder decoder=new BASE64Decoder();
            byte[] bytes = decoder.decodeBuffer(value);
            for(int i=0;i<bytes.length;i++){
                if(bytes[i]<0){
                    bytes[i]+=256;
                }
            }
            response.setContentType("image/jpg");
            ServletOutputStream out = response.getOutputStream();
            out.write(bytes);
            out.flush();
            out.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return "ok";
    }
}
