package com.example.demo.query;

import lombok.Data;

/**
 * @author ：lichuanbin
 * @since : 2021/4/8 17:16
 */
@Data
public class WebAuthQuery {
    private String userAccount;
    private String password;
}
