package com.example.demo.query;

import lombok.Data;

/**
 * @author ：lichuanbin
 * @since : 2021/4/1 15:45
 */
@Data
public class WebGoodsSelectQuery {
    /**
     * 当前页
     */
    private Integer currentPage;
    /**
     * 页内数量
     */
    private Integer pageSize;
    /**
     * 关键字
     */
    private String keyWord;
    /**
     * 商品id
     */
    private Long goodsId;
    /**
     * 商品状态
     */
    private Integer goodsStatus;
    /**
     * 首页显示 0-不显示 1-显示
     */
    private Integer isDisplay;
    /**
     * 游戏名称
     */
    private Long gameId;
    /**
     * 最低价
     */
    private Integer startPrice;
    /**
     * 最高价
     */
    private Integer endPrice;
    /**
     * 开始时间
     */
    private Integer startTime;
    /**
     * 最终时间
     */
    private Integer endTime;

}
