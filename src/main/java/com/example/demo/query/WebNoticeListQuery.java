package com.example.demo.query;

import lombok.Data;

/**
 * @author ：lichuanbin
 * @since : 2021/4/2 11:54
 */
@Data
public class WebNoticeListQuery {
    private Integer currentPage;
    private Integer pageSize;
    private Integer state;
}
