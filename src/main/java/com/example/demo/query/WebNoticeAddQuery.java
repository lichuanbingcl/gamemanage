package com.example.demo.query;

import lombok.Data;

/**
 * @author ：lichuanbin
 * @since : 2021/4/2 11:37
 */
@Data
public class WebNoticeAddQuery {
    /**
     * 公告标题
     */
    private String noticeTitle;
    /**
     * 公告副标题
     */
    private String noticeSubTitle;
    /**
     * 公告图片url
     */
    private String noticeImageUrl;
    /**
     * 公告正文
     */
    private String noticeContent;
}
