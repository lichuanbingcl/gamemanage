package com.example.demo.query;

import lombok.Data;

/**
 * @author ：lichuanbin
 * @since : 2021/4/1 18:27
 */
@Data
public class WebUserUpdateQuery {
    private Long userId;
    private String userAccount;
    private String password;
}
