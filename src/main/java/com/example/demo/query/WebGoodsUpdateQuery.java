package com.example.demo.query;

import lombok.Data;

import java.util.List;

/**
 * @author ：lichuanbin
 * @since : 2021/4/1 15:37
 */
@Data
public class WebGoodsUpdateQuery {
    /**
     * 商品id
     */
    private Long goodsId;
    /**
     * 商品标题
     */
    private String goodsName;
    /**
     * 商品价格
     */
    private Integer goodsPrice;
    /**
     * 首页显示
     */
    private Integer isDisplay;
    /**
     * 商品描述
     */
    private String goodsDescription;
    /**
     * 游戏id
     */
    private Long gameId;
    /**
     * 游戏类型id
     */
    private String typeName;
    /**
     * 图片路径
     */
    private List<String> images;
    /**
     * 商品状态
     */
    private Integer goodsStatus;
}
