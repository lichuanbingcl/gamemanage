package com.example.demo.query;

import lombok.Data;

/**
 * @author ：lichuanbin
 * @since : 2021/4/2 15:05
 */
@Data
public class WebBannerUpdateQuery {
    private Long bannerId;
    private String bannerImageUrl;
    private String bannerLinkUrl;
}
