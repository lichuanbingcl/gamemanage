package com.example.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.entity.Notice;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author ：lichuanbin
 * @since : 2021/4/1 11:47
 */
@Mapper
public interface NoticeMapper extends BaseMapper<Notice> {
}
