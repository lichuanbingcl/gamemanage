package com.example.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.entity.Image;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author ：lichuanbin
 * @since : 2021/4/1 11:46
 */
@Mapper
public interface ImageMapper extends BaseMapper<Image> {
}
