package com.example.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.entity.Banner;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author ：lichuanbin
 * @since : 2021/4/1 11:45
 */
@Mapper
public interface BannerMapper extends BaseMapper<Banner> {
}
